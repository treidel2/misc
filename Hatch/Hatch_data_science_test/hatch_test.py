#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 09:36:14 2017

@author: omri Bahat Treidel
"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

################ functions for Q 1c and 1d ###################

def is_val_numeric(x):
    try:
        float(x)
        return True
    except ValueError:
        return False

def is_column_numeric(col):
    if any(col.apply(is_val_numeric)):
        return True
    else:
        return False

def val_to_numeric(x):
    try:
        return float(x)
    except ValueError:
        return np.nan
        
def column_to_numeric(col):
    return col.apply(val_to_numeric)


def fix_numerical_data_errors(df):
    for cname in df.columns:
        if is_column_numeric(df[cname]):
            df.loc[:, cname] = column_to_numeric(df[cname])
        else:
            df.drop(cname, inplace=True, axis=1)
#################### helper functions for Q 1e  and 1f ##############

# this is a symmetric evaluation of the scale parmeter (MAD) which can be generalized 
# for asymmetric cases as well.
def robust_parameters(x):
    sel = np.isfinite(x)
    y = x[sel]
    med_y = np.median(y)
    dy = y - med_y
    scale = np.median(np.abs(dy))
    MAD_scale_factor = 1.4826 # scale facor to competability to standard deviation for Gaussian distribution
    if scale == 0:
       scale = np.nanpercentile(y, 75) - np.nanpercentile(y, 25)
    return med_y, MAD_scale_factor*scale

    
def reject_outliers(df, num_scale=3.0):
    try:
        for cname in df.columns:
            if (df[cname].nunique() > 10) and is_column_numeric(df[cname]):
                location, scale = robust_parameters(df[cname])
                inliers_sel = (np.abs(df[cname] - location) < num_scale*scale).values
                df.loc[~inliers_sel ,cname] = np.nan
    except:
        print cname, df[cname].describe
        
################### function for Q 1g #################

def num_unique_values_in_column(df):
    num_unique_values = []    
    for cname in df.columns:
        num_unique_values.append(df[cname].nunique())
    return np.array(num_unique_values)
        
def remove_uninformative_columns(df):
    """ Remove column with less than 2 valid values """
    col_sel = num_unique_values_in_column(df) < 2 # only 1 valid value doesn't contribute any information
#    print "removing columns", df.columns[col_sel]
    df.drop(df.columns[col_sel], axis=1, inplace=True)

####################### function for Q 1h ##################
    
def capitalise_and_remove_numbers(string):
    return "".join([char for char in string.upper() if not char.isdigit()])

#####################3 functions for Q1 i ################


def compute_tot_downtime(groups):
    # total downtime (time until repairment)
    areas = []
    tot_downtimes = []
    for area, vals in groups:
        areas.append(area)    
        tot_downtimes.append(np.sum(vals.downtime))
    
    return pd.Series(tot_downtimes, index=areas).sort_values(ascending=False)


def median_time_to_failure(groups):
    areas = []
    meddian_time_to_fail = []
    counts = []
    for area, vals in groups:
        vals.sort_values("Start", inplace=True)
        if len(vals) < 2: # cannot evaluate with only 1 repair 
            continue
        run_times = (vals["Start"].values[1:] - vals["Finish"].values[0:-1]).astype(float)/1e9/60
        areas.append(area)
        counts.append(len(vals))
        meddian_time_to_fail.append(np.median(run_times))
    return pd.DataFrame({"time":meddian_time_to_fail, "counts":counts}, index=areas).sort_values("time",ascending=True)

    
    
##################### functions for Q2 a ########################

def remove_sensors_with_many_missing_values(df, persentage_threshold=0.1):
    N = float(len(df))
    for cname in df.columns:
        num_valid_vals = df[cname].count()
        non_valid_percentage = 1.0 - num_valid_vals/N
        if non_valid_percentage > persentage_threshold:
#            print cname, num_valid_vals/N
            df.drop(cname, axis=1, inplace=True)

###################### function for Q4 a ###############

def corr_coeficients(df, target):
    sel_target = np.isfinite(target)
    corr = []
    sensors = []
    for c in df.columns:
        if is_column_numeric(df[c]):
            sel_c = np.isfinite(df[c]).values
            sel = sel_target & sel_c
            corr.append(np.corrcoef(target[sel], df.loc[sel, c].values)[0,1])
            sensors.append(c)
    return pd.Series(corr, index=sensors).sort_values(ascending=False)

            
            
#######################################################

path = "/home/omri/Hatch/Hatch_data_science_test"

fname1 = os.path.join(path, "PD1.csv")
pd1_dataset = pd.read_csv(fname1, sep=",")

# Q1a (~1 min)
print "%s rows and %s columns" % (pd1_dataset.shape)

# Q1b (~3-5 min)
fname2 = os.path.join(path, "PD2.csv")
pd2_dataset = pd.read_csv(fname2, sep=",")
pd2_dataset = pd2_dataset.rename(columns={"TimeStamp_y": "TimeStamp"})

# check that all times are identical
assert all(pd2_dataset.TimeStamp == pd1_dataset.TimeStamp)

merge_dataset = pd.merge(pd1_dataset, pd2_dataset, on="TimeStamp")
merge_dataset.loc[:, "TimeStamp"] = pd.to_datetime(merge_dataset.TimeStamp)
merge_dataset.sort_values(by="TimeStamp", inplace=True)
merge_dataset.set_index("TimeStamp", inplace=True)

# Q1c (~15 min)

# do it on a copy for now and will fix the acctul data in the next section
col_537TT033_PV_V_copy = merge_dataset["537TT033/PV.V"].copy()
col_537TT033_PV_V_fixed = column_to_numeric(col_537TT033_PV_V_copy)

## the generic case
fix_numerical_data_errors(merge_dataset)

# I find the request `As the end of this, all columns should contain only
# numeric data` very strange since some of the columns contains data that is not numerical at all
# it is descriptive words, for example `lance_tip_operation_mode` in the second dataset. 
# I am assuming that the intension was to convert 
# only the numerical data.  

# Q1d (~ 1 min)
print "the (10, 50, 90) percentiles are", np.nanpercentile(col_537TT033_PV_V_fixed, (10, 50, 90))

# Q1 e (~ 10 min)


col_322_PIT_112_PV_V_copy = merge_dataset["322-PIT-112/PV.V"].copy()

min_val, max_val = np.nanpercentile(col_322_PIT_112_PV_V_copy, (2.5, 97.5))
sel = (col_322_PIT_112_PV_V_copy > min_val) & (col_322_PIT_112_PV_V_copy < max_val)

#plt.figure()
#plt.hist(col_322_PIT_112_PV_V_copy[np.isfinite(col_322_PIT_112_PV_V_copy)], 200, histtype="step")
#plt.hist(col_322_PIT_112_PV_V_copy[sel], 1000, histtype="step")
# trying to trim 2.5% clearly leaves the very long tail that contain many outliers

# The distribution of the sensor "322-PIT-112/PV.V" is sharply peaked around ~100
# with 2 other minor modes that can be considered as outliers.
# We can reject the outliers using distance from the main mode (robust Z-score 
# implemented below, trim some percentage of values below 5% and above 95%)
# or some other number that we choose.

location, scale = robust_parameters(col_322_PIT_112_PV_V_copy)
# this is a robust version of z-score test:
# values more than 3 times the scale from the location are considered outliers
inliers_sel = (np.abs(col_322_PIT_112_PV_V_copy - location) < 3.0*scale).values
col_322_PIT_112_PV_V_copy[~inliers_sel] = np.nan

# on the contrary for of the sensors have complex multy modal distribution 
# for which "outliers" are ill defined. As an example let us look at sensor "537TT033/PV.V"
# for which the main mode contains merely ~65% of the values and have no obvious outliers.
# Moreover, trimming the distributions blindly will cause a problem later on when 
# sensors with more that 10% invalid values are discarded, so even 5% trimming 
# already leave us with 10% invalid values.  

#
#plt.figure()
#plt.hist(col_537TT033_PV_V_fixed[np.isfinite(col_537TT033_PV_V_fixed)], 200, 
#                                 histtype="step", normed=True)
#plt.title("density sensor 537TT033/PV.V")
##
#plt.figure()
#plt.hist(col_537TT033_PV_V_fixed[np.isfinite(col_537TT033_PV_V_fixed)], 200, 
#                                 histtype="step", cumulative=True, normed=True)

#plt.title("cumulative sensor 537TT033/PV.V")

# Q 1f (~ 5 min)

# We are using the same method as above: robust symmetric Z-score. 
# we cloud have used IQR as a scal parameter or use trimming remove values below 10% percentile
# and above 90. These have much lower breakdown point. 

# TODO: discuss binary variables
#doesnt make sense for small number of values which we choose somewhat arbitrarly as 10
inliers_dataset = merge_dataset.copy()
reject_outliers(inliers_dataset)

# the automated rejection of outliers if very complicated and does not make sense
# for an arbitrary distribution.

# Q 1g
# sensors must have at least 2 valid values to be informative
remove_uninformative_columns(merge_dataset)
#
## test that all remaning columns have more than 2 valid values 
assert all(num_unique_values_in_column(merge_dataset) >= 2)

# Q 1h

fname3 = os.path.join(path, "OD.csv")
od_dataset = pd.read_csv(fname3, sep=",")

# I find the requierment to remove al the numbers strange
# also, Since some of the Capital letters are acronims it doesn't make sense (to me) 
# to make them lowercase.
od_dataset.loc[:, "Description"] = od_dataset["Description"].apply(capitalise_and_remove_numbers) 

# Q 1i
#
od_dataset.loc[:, "Start"] = pd.to_datetime(od_dataset["Start"], dayfirst=True)
od_dataset.loc[:, "Finish"] = pd.to_datetime(od_dataset["Finish"], dayfirst=True)

# make sure all time are non-negative
assert all(od_dataset["Finish"] - od_dataset["Start"] >= np.timedelta64(0))

od_dataset["downtime"] = od_dataset["Finish"] - od_dataset["Start"]


#I am assuming "start" and "finish refer to the begining and ending of outage.
# Since the time=0 is unknown so the time to first failure
# cannot be computed. We can assume that the earliest time in the Pd1/Pd2 datatset 
# is the start time of operation but we cannot know for sure what was the sate prior to the time.
# the time to failure is the time from the finish_n - start_(n+1)

grouped = od_dataset.groupby("FailureMode")
    
tot_downtime = compute_tot_downtime(grouped)
    
print tot_downtime[0:3]

    
meddian_time_to_fail = median_time_to_failure(grouped)
print meddian_time_to_fail[0:3]

# Q 1j

# The key is to look at the feed rate during the repar time. We create an on/off
# function `unit` that has the value 0 dutin repairs. We expect the feed rate 
# to be zero during the `off` periods. 


T0 = pd.to_datetime(merge_dataset.index).values[0]
time = np.array((merge_dataset.index - merge_dataset.index[0]).astype(dtype='timedelta64[s]'))

#tmp = merge_dataset[["AS02_DATA_FROM_AS01/MtPV.V", "TimeStamp"]].sort_values(by="TimeStamp")

t_start = sorted(od_dataset["Start"].values)
t_finish = sorted(od_dataset["Finish"].values)

t_s = (t_start - T0).astype(float)/1e9
t_f = (t_finish - T0).astype(float)/1e9

t_r = sorted(np.concatenate((time, t_s, t_f)))

unit = np.ones(len(t_r))
for min_t, max_t in zip(t_s, t_f):
    i_s = np.argmin(np.abs(t_r - min_t))
    i_f = np.argmin(np.abs(t_r - max_t))
    unit[i_s:i_f] = 0

plt.figure()
plt.plot(time, merge_dataset["AS02_DATA_FROM_AS01/MtPV.V"].values, ".")
plt.plot(t_s, 42*np.ones(len(od_dataset)), "or")
plt.plot(t_f, 42*np.ones(len(od_dataset)), "xk")
plt.plot(t_r, 42*unit, "r-")
plt.title("process AS02_DATA_FROM_AS01/MtPV.V + outage", fontsize=18)
plt.legend(["AS02_DATA_FROM_AS01/MtPV.V", "Start", "Finish", "outage"], fontsize=18)
plt.xlabel("time from %s [min]" % T0, fontsize=18)
#plt.text(1.0e6,70 ,"(c)", fontsize=18)
#
## it seems like when outage is reported the feed rate is indeed zero. However, 
## it is very common for the feed rate to 0 when the system is reported as on.
#
#######################  Imputation  #####################

# before removing sensors with more than 10% missing values
# let us compute the number of missing values for sensor "537TT033/PV.V

N = float(len(merge_dataset))

sensor_537TT033_PV_V = merge_dataset["537TT033/PV.V"].copy()
print "percentage of invalid values %s" % (1.0 - sensor_537TT033_PV_V.count()/N)

# since sensor "537TT033/PV.V" have ~5 % invalid values 
# if we reject outliers we end up with ~ 34% invalid values 


# Q 2a
remove_sensors_with_many_missing_values(truncated_dataset)

# Q 2b
sel = np.isfinite(sensor_537TT033_PV_V)
truncated_dataset = merge_dataset[sel]

# Q 2c

sensor_322_PIT_112_PV_V = truncated_dataset["322-PIT-112/PV.V"].copy()
# select all the nan rows and replace with the mean
sel = np.isnan(sensor_322_PIT_112_PV_V).values
# here its done on a copy but it can be plugged back in to the data
truncated_dataset.loc[sel, "322-PIT-112/PV.V"] = np.nanmean(sensor_322_PIT_112_PV_V)

# Q 2d
interpolated_322_PIT_112_PV_V = sensor_322_PIT_112_PV_V.interpolate(method="time")
interp_sel = ~np.isfinite(sensor_322_PIT_112_PV_V.values)

plt.figure()
plt.plot(interpolated_322_PIT_112_PV_V[interp_sel].index, interpolated_322_PIT_112_PV_V.values[interp_sel],'.r')
plt.plot(interpolated_322_PIT_112_PV_V.index, sensor_322_PIT_112_PV_V.values,".k")
plt.legend(["interpolated", "valid data"])
plt.title("sensor 322-PIT-112/PV.V")

# Both knn and interpolation have a major drawback when the data is not contigueous 
# or not known to be. knn suffers from not taking into account the distance of the neighbours
# which can be overcome by weighting the neighbours by the distance.

# a better method than both of the above is to estimate the distribution from 
# the exsisting values (using Kernel density estimate for continueous varibles)
# and randomly sample from it to generate missing values. Also, the neighbours
# of a missing data point can be used as a prior which part of the distribution
# should be sampled 
 
# Q3a

# it is not clear weather this question should be answered including the operation
# performed in question 2. I am assuming that that is the case.


plt.figure()
plt.plot(truncated_dataset.index, truncated_dataset["537TT033/PV.V"].values, ".")
plt.title("Sensor 537TT033/PV.V")
plt.xlabel("time")

# Q3b

plt.figure()
plt.plot(truncated_dataset.index, truncated_dataset["AS02_DATA_FROM_AS01/MtPV.V"].values, ".")
plt.plot(truncated_dataset.index, 42*np.ones(len(truncated_dataset)), "r", lw=2)
plt.title("target feed rate for the process AS02_DATA_FROM_AS01/MtPV.V")
plt.xlabel("time", fontsize=16)
plt.ylabel("feed rate [tons/hour]", fontsize=16)
plt.legend(["feed rate", "target rate"])


feed_rate = truncated_dataset["AS02_DATA_FROM_AS01/MtPV.V"].values
feed_rate_sel = np.isfinite(feed_rate)


#Q 3c

target_rate = truncated_dataset["AS02_DATA_FROM_AS01/MtSP$From$AS01.V"].values
target_sel = np.isfinite(target_rate)
sel = feed_rate_sel & target_sel

props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

plt.figure()
plt.plot(target_rate[sel], feed_rate[sel], ".")
plt.ylabel("measured rate [tons/hour]", fontsize=16)
plt.xlabel("set point rate [tons/hour]", fontsize=16)
plt.plot(np.arange(78), np.arange(78), "r", lw=2)
plt.text(3, 75, "correlation coefficient %f" % np.corrcoef(target_rate[sel], feed_rate[sel])[0, 1], 
         fontsize=16, verticalalignment='top', bbox=props)
plt.legend(["measurments", "perfect: measured = set_point"])
plt.xlim(min(target_rate[sel]), max(target_rate[sel]))
plt.ylim(min(feed_rate[sel]), max(feed_rate[sel]))

#Q 3d

lance_pos = truncated_dataset["322-ZT-106/PV.V"].values
lance_sel = np.isfinite(lance_pos)
i_fail = np.nanargmin(lance_pos)

plt.figure()
plt.plot(truncated_dataset.index, feed_rate, ".")
plt.plot(truncated_dataset.index, np.ones(len(truncated_dataset))*42, "k-", lw=2)
plt.plot(truncated_dataset.index[:i_fail ], lance_pos[:i_fail ]/500, ".-")
plt.plot(truncated_dataset.index[i_fail: ], lance_pos[i_fail:]/500, ".-")
plt.xlabel("time", fontsize=18)
plt.legend(["feed rate", "set point = 42 tons/hour", "lance_position/500 before fail", 
"lance_position/500 failing"], fontsize=18)



plt.figure()
plt.plot(target_rate[sel][:i_fail ], feed_rate[sel][:i_fail ], ".")
plt.plot(target_rate[sel][i_fail: ], feed_rate[sel][i_fail: ], ".")
plt.plot(np.arange(78), np.arange(78), "r", lw=2)
plt.ylabel("measured rate [tons/hour]", fontsize=16)
plt.xlabel("set point rate [tons/hour]", fontsize=16)
plt.plot(np.arange(78), np.arange(78), "r", lw=2)
plt.legend(["before lance fail", "after lance fail", "perfect: measured = set_point"], loc=2)
plt.xlim(min(target_rate[sel]), max(target_rate[sel]))
plt.ylim(min(feed_rate[sel]), max(feed_rate[sel]))


# Q4 a

   
corr = corr_coeficients(truncated_dataset, truncated_dataset["AS02_DATA_FROM_AS01/MtPV.V"].values)
abs_correlation = pd.Series(np.abs(corr)).sort_values(ascending=False)

print abs_correlation[1:11]


############################################################################3

from sklearn.linear_model import LassoLarsIC, LinearRegression, HuberRegressor
from sklearn.model_selection import cross_val_score, cross_val_predict


sensor_names = list(abs_correlation.index[1:15])
X = truncated_dataset[sensor_names].values
sel_x = np.any(np.isnan(X), axis=1)
sel = ~sel_x & feed_rate_sel
valid_X = X[sel, :]

target = feed_rate[sel]

# L1 prior (regularization) on the vector of coeficient is approximate to
# sparsity condition i.e. set to 0 irrelevant coeficients.
lasso_bic = LassoLarsIC(criterion='bic', normalize=True)
lasso_bic.fit(valid_X, target)


abs_coef = abs(lasso_bic.coef_)
sorted_coef_inds = np.argsort(-abs_coef) # sort in descending order
best_5_inds = sorted_coef_inds[0:5]


# find which features are binary
num_unique_values = num_unique_values_in_column(truncated_dataset[sensor_names])
sel_binary = num_unique_values < 3
binary_features_inds = np.where(sel_binary)[0]


plt.figure()
plt.plot(abs_coef,".")
plt.plot(best_5_inds, abs_coef[best_5_inds],"rx", ms=10)
plt.plot(binary_features_inds, abs_coef[binary_features_inds],"+k", ms=10)
plt.title("magnitude of Lasso coeficients")
plt.legend(["best 15", "best 5", "binary"], loc=2)
plt.xlabel("feature index", fontsize=16)
plt.ylabel("coeficient value", fontsize=16)


plt.figure()
plt.plot(truncated_dataset.index, feed_rate, ".")
plt.plot(truncated_dataset.index, 42*truncated_dataset['AS01_DATA_TO_AS05/MtFdStt.Q'].values,"-r")
plt.legend(["feed rate", "AS01_DATA_TO_AS05/MtFdStt.Q"])
plt.xlabel("time", fontsize=16)

# Q4 b

linear = LinearRegression()
scores_linear = cross_val_score(linear, valid_X[:, best_5_inds], target, cv=43)
print("Accuracy 5 features: %0.4f (+/- %0.4f)" % (scores_linear.mean(), scores_linear.std() * 2))


predicted_linear = cross_val_predict(linear, valid_X[:, best_5_inds], target, cv=43)

plt.figure()
plt.plot(predicted_linear, target,".")
plt.plot(target, target, "r")
plt.title("Linear", fontsize=16)
plt.xlabel("predicted", fontsize=16)
plt.ylabel("measured", fontsize=16)


huber = HuberRegressor(epsilon=1.0)
scores_huber = cross_val_score(huber, valid_X[:, best_5_inds], target, cv=43)
print("Accuracy: %0.4f (+/- %0.4f)" % (scores_huber.mean(), scores_huber.std() * 2))

bins = np.linspace(0.75,1,10)
plt.figure()
plt.hist(scores_linear, bins, histtype='step')
plt.hist(scores_huber, bins, histtype='step')
plt.title("scores")
plt.legend(["model (i) - Gaussian", "model (ii) - exponential"], loc=2)
plt.xlabel("Score")

predicted_huber = cross_val_predict(huber, valid_X[:, best_5_inds], target, cv=43)

plt.figure()
plt.plot(predicted_huber, target,".")
plt.plot(target, target, "r")
plt.title("Huber", fontsize=16)
plt.xlabel("predicted", fontsize=16)
plt.ylabel("measured", fontsize=16)


plt.figure()
plt.hist(predicted_linear - target, 200, histtype='step')
plt.hist(predicted_huber - target, 200, histtype='step')