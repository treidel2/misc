# -*- coding: utf-8 -*-
"""
Created on Sat Mar 19 07:35:30 2016

@author: omri
"""

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from lmfit import minimize, Parameters, Parameter, report_fit

qe = 1.60217662e-19
kB = 1.38064852e-23
h = 6.62607004e-34
me = 9.10938356e-31
m_eff = 0.2*me

def linear_model(params, t, z, weights):
    p = params.valuesdict()
    model = (p['a']*t + p['b']) 
    res = model - z
    return res*weights


def fcn2min(params, t, z, weights):
    p = params.valuesdict()

    model = p['C']*(1 - np.exp(-t/p['tau1'])) + p['A']*np.exp(-t/p['tau2']) - p['B'] 
    res = model - z
    return res*weights        


def eval_model(p, t):
    return p['C']*(1 - np.exp(-t/p['tau1'])) + p['A']*np.exp(-t/p['tau2']) - p['B']
#
#def fcn2min(params, t, z, weights):
#    p = params.valuesdict()
#
#    model = (p['R0'] + p['R1']*(1 - np.exp(-(t)/p['tau1'])) 
#            + p['R2']*(1 - p['t2']*np.exp(-(t)/p['tau2'])) ) 
#    res = model - z
#    return res*weights       
    
def get_params(result):
    vals = {}    
    for p in result.params.values():
        vals[p.name] = p.value
    return vals

def err_log(x, err_x):
    return np.abs(err_x/x)

    
path = '/home/omri/misc/GaN/data/'
#fname = os.path.join(path, 'A01982_01R01_100mA.csv')
fname = os.path.join(path, '200V_100mA_wErrors.csv')
data = pd.read_csv(fname, sep=',')



R0_vals = np.array([1.22147822700255,1.36906352359008,1.60155896790506,1.65693378538206,1.63224005990923,
                    1.84015480660649,1.89143504344105,1.97111049929347,1.88949757998518,1.89722277582364,
                    2.06094404891047,1.92542072358141,2.16724622889186,2.19891282168230,2.18803303505261,
                    2.16738709462460,2.26039840967121,2.21104582984881])

tt = np.arange(0.001, 2.5, 0.0001)

temperatures = [25, 50, 75, 90, 100, 110, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175]
temperatures = map(str, temperatures)

Temp = []
Tau1 = []
errTua1 = []
Tau2 = []
errTua2 = []
colors = np.random.rand(len(np.unique(data.V)), 3)

fit_params = []

for r, T in zip(R0_vals, temperatures):
    if float(T) < 120:
        continue
     
    
    plt.figure("T=%s" % T)
    leg= []
    
    sel = np.array([T in c for c in data.columns])
    Tcols = data.columns[sel]
    
    if Tcols[0].startswith("R"):
        r_col = Tcols[0] 
        e_col = Tcols[1]
    else:
        e_col = Tcols[0] 
        r_col = Tcols[1]
    
    for i, v in enumerate(np.unique(data.V)):
        
        leg.extend(["V=%s" % v, "V=%s" % v])
        
        sel = data.V == v        
        t = data.time[sel].values
        R = data.loc[sel, r_col].values
        W = 1/(data.loc[sel, e_col].values)
        
        C0 = 1.1*max(R)
        params = Parameters()
        params.add('A',    value = C0 - R[-1] + R[0])
        params.add('B',    value = C0 - R[-1])
        params.add('C',    value = C0, max=1.5*max(R))        
        params.add('tau1',  value = 0.1*t[np.argmax(R)])
        params.add('tau2',  value = 0.05, min=0.001, max=5.0)
        
        print "tau1 initial", 0.1*t[np.argmax(R)]

        result = minimize(fcn2min, params, args=(t, R, W))
        report_fit(result.params, show_correl=0)

        Temp.append(T)

        Tau1.append(result.params["tau1"].value)
        errTua1.append(result.params["tau1"].stderr)        
        
        Tau2.append(result.params["tau2"].value)
        errTua2.append(result.params["tau2"].stderr)                
        
        pars_vals = get_params(result)
        fit_params.append([pars_vals['A'], pars_vals['B'],pars_vals['C'],pars_vals['tau1'],pars_vals['tau2']])         
        
        final = R + result.residual            
        # write error report
        print "\n report for T=%s, V=%s \n" %(T, v)
        report_fit(result.params, show_correl=0)
                          
        plt.errorbar(np.log10(t), R, yerr=1/W, fmt = "o", color=colors[i])
        plt.plot(np.log10(tt), eval_model(pars_vals, tt), color=colors[i, :])            
        plt.title("Temperature=%s" % T) 
        plt.legend(leg)


df = pd.DataFrame(fit_params, columns=['A','B','C','tau1','tau2'])
df.loc[:,"Temterature[C]"] = Temp
df.to_csv("model_parameters.csv", sep=',', index=False)


Temp = np.array(Temp).astype(float) + 273.14

Tau1 = np.array(Tau1)
errTua1 = np.array(errTua1)

Tau2 = np.array(Tau2)
errTua2 = np.array(errTua2)

err1 =  err_log(Tau1, errTua1)
w1 = 1/(err1)

err2 =  err_log(Tau2, errTua2)
w2 = 1/(err2)


gamma = 2*np.sqrt(3)*(2.0*np.pi)**1.5 * kB**2 * m_eff * h**(-3)

X = qe/(kB*Temp)
Y1 = np.log(gamma*Tau1*Temp**2)
Y2 = np.log(gamma*Tau2*Temp**2)

params = Parameters()
params.add('a',    value = 0.5, min=0.0, max=2.0)
params.add('b',    value = 50.0, min=30.0, max=70.0)

result1 = minimize(linear_model, params, args=(X, Y1, w1))
a1 = result1.params['a'].value
b1 = result1.params['b'].value
err_a1 = result1.params['a'].stderr
err_b1 = result1.params['b'].stderr

report_fit(result1.params)

params = Parameters()
params.add('a',    value = 0.5, min=0.0, max=2.0)
params.add('b',    value = 50, min=5.0, max=80.0)
result2 = minimize(linear_model, params, args=(X, Y2, w2))
a2 = result2.params['a'].value
b2 = result2.params['b'].value
err_a2 = result2.params['a'].stderr
err_b2 = result2.params['b'].stderr


report_fit(result2.params)


plt.figure()
plt.errorbar(X, Y1, yerr=err1, fmt=".k")
plt.errorbar(X, Y2, yerr=err2, fmt=".m")
plt.plot(X, a1*X + b1,"-k", lw=2)
plt.plot(X, a2*X + b2,"-m", lw=2)
plt.show()

df = pd.DataFrame(np.array([Temp, Tau1, err1, Tau2, err2]).T, 
             columns=['Temperature[K]', 'tau1[s]', 'std_log(tau1)', 'tau2[s]', 'std_log(tau2)'])
df.to_csv("tau_out.csv" ,sep=',',index=False) 

with open("linear_fit_parameters.csv", "w") as f:
    f.write("tau1: \n")
    f.write("a = %s \n" %a1)
    f.write("err_a = %s \n" %err_a1)
    f.write("b = %s \n" %b1)
    f.write("err_b = %s \n" %err_b1)
    
    f.write("tau2: \n")
    f.write("a = %s \n" %a2)
    f.write("err_a = %s \n" %err_a2)
    f.write("b = %s \n" %b2)
    f.write("err_b = %s \n" %err_b2)