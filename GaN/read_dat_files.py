# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 18:17:28 2016

@author: omri
"""

import pandas as pd
import numpy as np
import os
from glob import glob


ON_CURRENT_MIN = 7.01e-6
ON_CURRENT_MAX = 9.31e-6

UDS_ON_MIN = 7.01e-6
UDS_ON_MAX = 9.31e-6

DRAIN_MIN = -1.13e-6
DRAIN_MAX = -9.0e-8

CLAMP_MIN = 7.01e-6
CLAMP_MAX = 9.31e-6


def replace_dat_with_csv(filename):
    with open(filename, 'r') as f:
        name = f.readline().split('.dat')[0]
        header =  f.readline().split(' ')
        columns = header[0] + '\n' 
        csv_name = name + '.csv' 
        data = f.read()
        with open(csv_name, 'w') as g:
            g.write(columns)        
            g.write(data)    
    return name 


def compute_stats(data, name):
    time = data['TIME'].values
    on_current_sel  = (ON_CURRENT_MIN <= time)  & (time <= ON_CURRENT_MAX)
    uds_on_sel      = (UDS_ON_MIN <= time)      & (time <= UDS_ON_MAX)
    uds_off_sel     = (DRAIN_MIN <= time)       & (time <= DRAIN_MAX)
    clamp_sel       = (CLAMP_MIN <= time)       & (time <= CLAMP_MAX)
    
    on_current_mean = data.loc[on_current_sel, ['DRAIN_CURRENT']].values.mean()
    on_current_std  = data.loc[on_current_sel, ['DRAIN_CURRENT']].values.std()
    
    uds_on_mean = data.loc[uds_on_sel, ['DRAIN_VOLTAGE']].values.mean()
    uds_on_std  = data.loc[uds_on_sel, ['DRAIN_VOLTAGE']].values.std()

    uds_off_mean = data.loc[uds_off_sel, ['DRAIN_VOLTAGE']].values.mean()
    uds_off_std  = data.loc[uds_off_sel, ['DRAIN_VOLTAGE']].values.std()
    
    clamp_mean = data.loc[clamp_sel, ['CLAMP']].values.mean()
    clamp_std  = data.loc[clamp_sel, ['CLAMP']].values.std()
    
    R = clamp_mean/on_current_mean
    dR = R * np.sqrt( (clamp_std/clamp_mean)**2 + (on_current_std/on_current_mean)**2 )    
    print dR
    print R
    return ([name, uds_off_mean, clamp_mean, on_current_mean, uds_on_mean, 
             uds_off_std, clamp_std, on_current_std, uds_on_std, dR])


def extract_metadata(name):
    metadata = name.split('_')
    temperature = float(metadata[0].split('o')[0])
    voltage = float(metadata[1].split('V')[0])
    current = float(metadata[2].split('mA')[0])
    some_sort_of_time = int(metadata[3].split('ms')[0])
    return [temperature, voltage, current, some_sort_of_time]

    
input_dir = '/home/omri/misc/GaN/data/'
output_name = os.path.join(input_dir, 'out.csv')
input_files_list = glob(input_dir + '*.dat')

stats_all = []
for filename in input_files_list:
    name = replace_dat_with_csv(filename)   
    data = pd.read_csv(name + '.csv', sep='\t')
    stats = compute_stats(data, name)
    metadata = extract_metadata(name)
    stats_all.append(stats + metadata)


new_cols = ['file', 'uds_off_mean', 'clamp_mean', 'on_current_mean', 'uds_on_mean', 
            'uds_off_std', 'clamp_std', 'on_current_std', 'uds_on_std',
            'temperature[oC]', 'voltage[V]', 'current[mA]', 'time[ms]', 'dR']

             
stats_all = pd.DataFrame(stats_all, columns=new_cols)
stats_all = stats_all.sort('time[ms]')
stats_all.to_csv(output_name, sep=' ', index=False)

print "done"
