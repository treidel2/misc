# -*- coding: utf-8 -*-

import glob
import numpy as np
import pandas as pd
import os
from shutil import copyfile, rmtree

import sys
current_directory = os.getcwd()  # get current directory
HOMEPATH = current_directory.split('anna_phd')[0]  # get the user home path
print "HOMEPATH is:", HOMEPATH     
sys.path.append(os.path.join(HOMEPATH, "anna_phd/"))
import utils


"""This file renames all the columns in the original files, changes the date formats
to be consistent, changes the values for ca_type in skin cancer so they are not the
same as the values in ca_type for non skin cancer. It then merges skin and non skin to make 
cancer. Cancer is merged with maindeid to make cancer_maindeid.
It DOES NOT remove people who had a cancer prior to transplant or take paediatric only"""

# change columns names according to a dictionary

cohistory_name_pairs = (
('id', 'id'),
('codate', 'cohistory_date'),	
('lung', 'lung_cohistory'),
('coronary', 'coronary_cohistory'),
('pvd', 'pvd_cohistory'),
('cvd', 'cerebrovascular_cohistory'),
('diabetes', 'diabetes_cohistory'),
('currpostcode', 'postcode_at_date_cohistory')
)

induction_name_pairs = (
('id', 'id'),
('graftno', 'graft'),
('monodate', 'induction_date'),
('agent', 'induction_agent'),
('reason', 'induction_reason'),
)

drug_name_pairs = (
('id', 'id'),
('graftno', 'graft'),
('drug', 'drug'), 
('dose0m', 'dose0m'),
('dose1m', 'dose1m'),
('dose2m', 'dose2m'),
('dose3m', 'dose3m'),
('dose6m', 'dose6m'),
('dose1y','dose1y'),
)

nonskin_name_pairs = ( # pair of (old_name, new_name)
('sequno', 'nonskin_ca_no'),
('catype', 'ca_type'),
('cadate', 'ca_date'),
('primdescr', 'nonskin_prim_site'),
('castage', 'ca_stage'),
('lympdate', 'lymph_spread_date'),
('systdate', 'metastasis_date'),
('recurdate', 'local_recur_nonskin_date'),
('causprd', 'nonskin_cancer_cause_eskd'),
('causdeath', 'cancer_cause_death')
)

  
skin_name_pairs = (
('id', 'id'),
('catype', 'ca_type'),
('postdate', 'ca_date'),
('lympdate', 'lymph_spread_date'),
('systdate', 'metastasis_date'),
('causdeath', 'cancer_cause_death'),
('predate', 'predate'),
('dxdate', 'dxdate'),
)

weight_name_pairs = (
('id', 'id'),
('graftno', 'graft'),
('wttx'   , 'wt_tx'),
('wt5y'   , 'wt_5yr'),
('wt10y'  , 'wt_10yr'),
('wt15y', 'wt_15yr'),
('wt20y', 'wt_20yr'),
('wt25y', 'wt_25yr'),
('wt30y', 'wt_30yr'),
('wt35y', 'wt_35yr'),
('wt40y', 'wt_40yr')
)

rejection_name_pairs = (
('id', 'id'),
('graftno', 'graft'),
('rejseq = rejection episode number', 'rejection_number'),
('rejdate', 'rejection_date'),
('biopsy  c = clinical suspocian, d = delayed graft function, p= protocol N - no', 'rejection_biopsy'),
)

virology_name_pairs = (
('id', 'id'),
('graftno', 'graft'),
('ddonid', 'ddonid'),
('don_cmv', 'don_cmv'),
('don_ebv', 'don_ebv'),
)

maindeid_name_pairs = (
('graftno', 'graft'),
('donsourc', 'donor_source'),
('txdate', 'transplant_date'),
('age', 'age_transplant'),
('misa', 'misa'),
('misb', 'misb'),
('misdr', 'misdr'),
('misdq', 'misdq'),
('cmv', 'cmv'),
('ebv', 'ebv'),
('donage', 'donor_age'),
('donsex', 'donor_sex'),
('causfail', 'graft_fail_reason'),
('faildate', 'transplant_fail_date'),
('maxcyto', 'max_pra'),
('sc1m', 'creatinine_1_mo'),
('sc6m', 'creatinine_6_mo'),
('sc1y', 'creatinine_12_mo'),
('graftsus', 'graft_functioning_at_death'),
('deathdate', 'death_date'),
('sex', 'sex'),
('race', 'race'),
('disease', 'disease'),
('height', 'height_rrt'),
('weight', 'weight_rrt'),
('cig', 'cig'),
('country', 'country_birth'),
('postcode', 'postcode_rrt'),
('rxdate', 'treatment_rrt_date'),
('agerrt', 'age_rrt')
)


renaming_dict = { 
'cohistory.csv': cohistory_name_pairs,
'induction.csv': induction_name_pairs,
'drug.csv'     : drug_name_pairs,
'nonskin.csv'  : nonskin_name_pairs,
'maindeid.csv' : maindeid_name_pairs,
'skin.csv'     : skin_name_pairs,
'weight.csv'   : weight_name_pairs,
'rejection.csv': rejection_name_pairs,
'virology.csv' : virology_name_pairs,
}


WORKING_PATH = os.path.join(HOMEPATH, 'anna_phd/cancer/working_data')
ORIGINAL_PATH = os.path.join(HOMEPATH, 'anna_phd/cancer/original_data_cancer') 


class InconsistantDataError(Exception):
    pass


def copy_data_files():
    """ copy all the original data files to a new directory and modifiying the extension """
    rmtree(WORKING_PATH)
    print "Removing WORKING_PATH:", WORKING_PATH   
    os.mkdir(WORKING_PATH)    
    files = glob.glob(ORIGINAL_PATH + '/*.org.csv')
    print "Copy original datat files"
    for f in files:
        new_file = os.path.join(WORKING_PATH, os.path.basename(f).split('.')[0] + '.csv')        
        copyfile(f, new_file) 


def fix_induction_date_format():
    """ this simply sort out the dates in induction since it is the only files with date in the format dd/mm/yy """
    data = pd.read_csv(os.path.join(ORIGINAL_PATH, 'induction.org.csv'))
    monodates = []
    for v in data['monodate']:
        if isinstance(v, str):
            monodates.append(utils.dd_mm_to_mm_dd(v))
        else:
            monodates.append(v)
    data.loc[:, 'monodate'] = monodates
    data.to_csv(os.path.join(WORKING_PATH, 'induction.csv'), sep=',', index=False)
    

def fix_date_format_all():
    """ change the date format to 4 digit year """
    files = glob.glob(WORKING_PATH + '/*.csv')
    for f in files:
        data = pd.read_csv(f, sep=',')
        utils.four_digit_all(data).to_csv(f, sep=' ', index=False)


def rename_columns():
    for filename, new_names in renaming_dict.items():
        print filename
        filename = os.path.join(WORKING_PATH, filename)
        df = pd.read_csv(filename, sep = ' ')
        df = utils.rename_columns(df, new_names).to_csv(filename, sep=' ', index=False)


def replace_skin_cancer_codes():
    filename = os.path.join(WORKING_PATH, 'skin.csv')
    data = pd.read_csv(filename, ' ')
    selection = data.ca_date.notnull()    
    skin_ca_type_pairs = [(4, 14), (5, 15), (6, 16), (7, 17), (10, 18), (11, 19), (12, 20), (13, 21), (14, 22), (15, 23)]
    data.loc[:, 'ca_type'] = utils.replace_integer_pairs(skin_ca_type_pairs, data.ca_type)
    data[selection].to_csv(filename, ' ', index=False)


def merge_cancer_files():
    nonskin = pd.read_csv(os.path.join(WORKING_PATH, 'nonskin.csv'), sep=' ')
    nonskin = utils.convert_all_to_datetime(nonskin)
    non_skin_type_sel = nonskin.ca_type.notnull()
    skin = pd.read_csv(os.path.join(WORKING_PATH, 'skin.csv'), sep=' ')
    skin = utils.convert_all_to_datetime(skin)
    cancer = pd.concat([skin, nonskin.loc[non_skin_type_sel] ], ignore_index=True)
    cancer.to_csv(os.path.join(WORKING_PATH, 'cancer.csv'), sep=' ', index=False)


def inner_merge_cancer_maindeid():
    maindeid = pd.read_csv(os.path.join(WORKING_PATH, 'maindeid.csv'), sep = ' ')
    maindeid = utils.convert_all_to_datetime(maindeid)
    cancer = pd.read_csv(os.path.join(WORKING_PATH, 'cancer.csv'), sep=' ')
    cancer = utils.convert_all_to_datetime(cancer)
    pd.merge(maindeid, cancer, on='id', how='inner').to_csv(os.path.join(WORKING_PATH, 'cancer_maindeid.csv'), sep=',', index=False)


def preprocessing():
    copy_data_files()
    fix_induction_date_format()
    fix_date_format_all()
    rename_columns()
    replace_skin_cancer_codes()
    merge_cancer_files()
    inner_merge_cancer_maindeid()
    
if __name__ == "__main__":
    preprocessing()