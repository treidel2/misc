# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 19:16:00 2016

@author: omri
"""

import numpy as np
import pandas as pd
import datetime


CENSUS = pd.Timestamp('2013/12/31')

def create_replacement_mapping(values_string, new_value):
    vals = values_string.split(', ')
    mapping = []
    for i in map(int, vals):
        mapping.append((i, new_value))
    return mapping

def keep_ids(df, selection):
    """ keep all ids corresponding to a given selection
    Args:
        df (DataFrame):
        selection (np.array(bool)):

    Returns:
        DataFrame: a dataframe with only the ids selected
    """
    id_to_keep = df.loc[selection, "id"].unique()
    grouped = pd.groupby(df, 'id')
    tmp = []
    for k,v in grouped:
        if k in id_to_keep: tmp.append(v)
    return pd.concat(tmp)

def select_deep_label(df, label, desird_level):
    """Select a label from a deeper level of a multiIndex
    
    Args:
        df (DataFrame): multiIndex DataFrame
        label (str): the desired label
        desird_level (str): the name of the level
    
    Returns:
        Dataframe: A reduced dataframe with the desired label
        
    """
    return df.xs(label, level=desird_level)


def create_multi_index(data, columns_index):
    """Create a dataframe from a data file with multiIndex
    
    Args:
        data (pandas.DataFrame): a dataframe for index modification
        columns_index (list[str]): list of the names of the columns for the multiIndex
        
    Returns:
        Dataframe: a dataframe with a multiindex as requested
    
    """
    grouped = data.groupby(columns_index)
    m_index = pd.MultiIndex.from_tuples(grouped.groups.keys(), names=columns_index)
    org_index = np.array(grouped.groups.values()).squeeze()
    tmp = data.loc[org_index]
    tmp.drop(columns_index, axis=1, inplace=True)
    return pd.DataFrame(tmp.values, index = m_index, columns=tmp.columns).sort()


def get_date_columns_names(data):
    """Get the names of the columns that ends with 'date'
    
    Args:
        data (pandas.DataFrame): data coming from a file that may contain dates
        
    Returns:
        list[str]: the names of the columns that end with 'date'
    """
    sel = [c.endswith('date') for c in data.columns]
    return data.columns[sel]


def replace_date_with_year(data):
    """Takes a dataFrame and replace the date with the year if the column name endswith 'date' 
    
    Args:
        data (DataFrame): original dataframe
    
    Returns:
        DataFrame: A new dataframe with the year rather than a date 
    
    """
    date_columns_names = get_date_columns_names(data)
    date_cols = data[date_columns_names].values
    for line in date_cols:
        for i in xrange(len(date_columns_names)):
            if isinstance(line[i], str):
                line[i] = line[i].split('/')[-1]
    
    data.loc[:, date_columns_names] = date_cols
    return data


class YearError(Exception):
    pass

  
def four_digit_date_format(date_string, min_year_1900=40):
    """Ensures that a date is in 4 digit format
    
    Args:
        date_string (str): the date to be converted to a year with 4 digits format
        
    Returns:
        (datetime.date): the date with the year in 4 digit format 
    """
    month, day, year = date_string.split('/')
    
    if len(year) == 2:
        year = int(year)
        if year >= min_year_1900:
            year += 1900
        else:
            year += 2000
    elif len(year) == 4:
        pass
    else:
        print date_string
        raise YearError("Year %s is niether in 2 or 4 digit format", year)

    return datetime.date(int(year), int(month), int(day))


def four_digit_date_format_test_0():
    for y in range(1960, 2014):
        for m in range(1,13):
            for d in range(1,29):
                date_str = str(m).zfill(2) + '/' + str(d).zfill(2) + '/' + str(y)
                assert four_digit_date_format(date_str) == datetime.date(y,m,d) 
    

def four_digit_date_format_test_2000():
    for y in range(0, 14):
        for m in range(1,13):
            for d in range(1,29):
                date_str = str(m).zfill(2) + '/' + str(d).zfill(2) + '/' + str(y).zfill(2)
                
                if four_digit_date_format(date_str) != datetime.date(2000+y,m,d) :
                    print four_digit_date_format(date_str)
                    print datetime.date(y,m,d)
                    raise ValueError

                    
def four_digit_date_format_test_1900():
    for y in range(60, 99):
        for m in range(1,13):
            for d in range(1,29):
                date_str = str(m).zfill(2) + '/' + str(d).zfill(2) + '/' + str(y).zfill(2)
                
                if four_digit_date_format(date_str) != datetime.date(1900+y,m,d) :
                    print four_digit_date_format(date_str)
                    print datetime.date(y,m,d)
                    raise ValueError


def four_digit_date_format_test_no_zfill():
    for y in range(60, 99):
        for m in range(1,13):
            for d in range(1,29):
                date_str = str(m) + '/' + str(d) + '/' + str(y).zfill(2)
                
                if four_digit_date_format(date_str) != datetime.date(1900+y,m,d) :
                    print four_digit_date_format(date_str)
                    print datetime.date(y,m,d)
                    raise ValueError
                    
                
def four_digit_all(data):
    """Takes a dataFrame and replace all the dates with 4 digit format 
    
    Args:
        data (DataFrame): original dataframe
    
    Returns:
        DataFrame: A new dataframe with the date in 4 digit format as datetime.date 
    
    """
    date_columns_names = get_date_columns_names(data)
    date_cols = data[date_columns_names].values
    drop_index = []
    for j, line in enumerate(date_cols):
        for i in xrange(len(date_columns_names)):
            if isinstance(line[i], str):
                try:
                    line[i] = four_digit_date_format(line[i])
                except YearError:
                    drop_index.append(j)
    data.loc[:, date_columns_names] = date_cols
    return data.drop(drop_index, axis=0)  


def create_date_string(x, y, z):
    return str(x) + '/' + str(y) + '/' + str(z)


def dd_mm_to_mm_dd(date_string):
    """Change the date format from day/month -> month/day 
    
    Args:
        date_string (str): the date to be converted to a year with 4 digits format
        
    Returns:
        (str): the date with the year in 4 digit format 
    """
    day, month, year = date_string.split('/')
    
    return create_date_string(month, day, year)


def find_valid_string(column):
    """check if the values in column are of type string
    
    Args:
        column (list/numpy.array/pandas.Series): an iterable that holds values of interest
        
    Returns:
        numpy.array: True values when the entry is a string 
    
    """
    return np.array([isinstance(val, str) for val in column])


def replace_integer_pairs(pairs, column):    
    org = column.values.copy()
    for pair in pairs:
         org[column.values == pair[0]] = pair[1]    
    return org

  
  
def rename_columns(df, names_mapping):
    """
    Args:
        df (pandas.DataFrame): a DataFrame for renaming columns
        names_mapping (tuple): mapping between old and new names
    
    Returns:
        (pandas.DataFrame): a DataFrame with new columns names
    
    Example:
        names_map = ( (old_name_1, new_name_1), (old_name_2, new_name_2) )
        cancer = rename_columns(cancer, names_map)
    """
    columns_dict = {}
    for old, new in names_mapping:
        columns_dict.update({old:new})
    
    return df.rename(columns = columns_dict)


def string_do_datetime(string):
    #FIXEME: is not needed anymore
    y,m,d = string.split('-')
    return datetime.date(int(y), int(m), int(d))
    

def convert_column_to_datetime(dataframe, column_names):
    """    
    Args:
        dataframe (pandas.DataFrame): the dataframe you want to access      
        column_names (list[str]): a list of the column names
    
    Example:
        cancer = convert_column_to_datetime(cancer, ['transplant_date', 'ca_date'])
    """
    dataframe.loc[:, column_names] = dataframe.loc[:, column_names].apply(pd.to_datetime)
    return dataframe


def convert_all_to_datetime(df):
    sel = [c.endswith("date") for c in df.columns]
    df = convert_column_to_datetime(df, df.columns[sel])
    return df


def time_between_dates(dataframe, early_column, late_column, new_column):
    """
    Args:
        dataframe (pandas.DataFrame): the dataframe you want to access      
        early_column: earliest date
        late_column: latest date
        new_column: new column name
    
    Returns:
        column
    
    Example:
        
    """
    dataframe = convert_column_to_datetime(dataframe, [early_column, late_column])
    dataframe.loc[:, new_column] = (dataframe[late_column] - dataframe[early_column]).astype(dtype='timedelta64[D]')
    return dataframe



def time_to_census(dataframe, early_column, new_column):
    """
    Args:
        dataframe (pandas.DataFrame): the dataframe you want to access      
        early_column: earliest date
        census_date: census date
        new_column: new column name
    
    Returns:
        column
    
    Example:
        
    """
    dataframe.loc[:, [early_column]] = convert_column_to_datetime(dataframe, [early_column])
    dataframe.loc[:, new_column] = (CENSUS - dataframe[early_column]).astype(dtype='timedelta64[D]')
    return dataframe

def age_at_event(dataframe, age_transplant, time_to_event, new_column):# this returns a column with datatype as objects, not integers
    """
    Args:
        dataframe (pandas.DataFrame): the dataframe you want to access      
        age_transplant: earliest date
        time_to_event: latest date
        new_column: new column name
    
    Returns:
        column
    
    Example:
      graft = utils.age_at_event(graft, "age_transplant", "time_to_nonskin_cancer", "age_nonskin_cancer")
  
    """
    

    dataframe.loc[:, new_column] = "" # create an empty columns
    nan_sel = np.isnan(dataframe[time_to_event].values)
    dataframe.loc[~nan_sel, new_column] = (dataframe.loc[~nan_sel, age_transplant] + (dataframe.loc[~nan_sel, time_to_event]/365)).astype(dtype='int64')
    return dataframe
